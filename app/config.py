from pydantic import BaseSettings
import json

class Settings(BaseSettings):
	# Generic app info
    app_name: str = "Connection Profile API"
    app_description: str = "Connection Profile API"
    app_version: str = "1.0.0"

    ccp_path: str = "/code/connection-profile.json"

settings = Settings()
