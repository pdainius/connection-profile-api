from fastapi import FastAPI, status
from fastapi.responses import JSONResponse

from app import config

import json

app = FastAPI(
    title=config.settings.app_name,
    description=config.settings.app_description,
    version=config.settings.app_version,
    docs_url=None,
    redoc_url=None
)

def read_ccp_file() -> dict:
    with open(config.settings.ccp_path) as f:
        data = json.load(f)

        return data
    return {}

@app.get("/api/v1/ccp/4ebd0208-8328-5d69-8c44-ec50939c0967")
async def get_ccp_content():
    try:
        data = read_ccp_file()
        return JSONResponse(
            content=data
        )
    except Exception as e:
        return JSONResponse(
            content={"message": str(e)},
            status_code=status.HTTP_400_BAD_REQUEST
        )